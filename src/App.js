// import cat from './static/cat.gif'
// import land from './static/land.png'
// import moon from './static/moon.png'

import TextBlock from './textblock'

import { Parallax, ParallaxLayer } from '@react-spring/parallax';

//cover or contain

function App() {
  return (
    <div className="App">
      <Parallax pages={2} style={{top:'0', left: '0'}} className="animation">
        <ParallaxLayer offset={0} speed={0}>
          <div class="animation_layer parallax" id="sky"></div>
        </ParallaxLayer>
        <ParallaxLayer offset={0} speed={-0.25}>
          <div class="animation_layer parallax" id="backmountains"></div>
        </ParallaxLayer>
        <ParallaxLayer offset={0} speed={-0.75}>
          <div class="animation_layer parallax" id="name"><h2>MIMI LAU</h2></div>
        </ParallaxLayer>
        <ParallaxLayer offset={0} speed={-0.19}>
          <div class="animation_layer parallax" id="field"></div>
        </ParallaxLayer>
        <ParallaxLayer offset={0} speed={-0.15}>
          <div class="animation_layer parallax" id="cliffs"></div>
        </ParallaxLayer>
        <ParallaxLayer offset={0} speed={0}>
          <div class="animation_layer parallax" id="trees"></div>
        </ParallaxLayer>
        <ParallaxLayer offset={0.99} speed={0} factor={1}>
          <TextBlock />
        </ParallaxLayer>
      </Parallax>
    </div>
  );
}

export default App;
